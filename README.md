---
layout: page
title: Strupler2013b--Hethitische_Keramik
---

Preprint with code and data of: 

 Strupler 2016 “ ‘Dater d’après le cachet’: Une approche méthodologique
  pour visualiser les cachets circulaires hittites", in Butterlin P., Patrier J.
    and Quenet P. (Eds), \emph{Milles et une empreintes. Un Alsacien en Orient. Mélanges
    en l'honneur du 65e anniversaire de Dominique Beyer}, 

 [preprint : https://hal.archives-ouvertes.fr/hal-1485166](https://hal.archives-ouvertes.fr/hal-1485166)

## Licence

  - Text, data and code: Strupler Néhémie CC BY-SA (http://creativecommons.org/licenses/by-sa/4.0/)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"></span> <a xmlns:cc="http://creativecommons.org/ns#" href= "https://hal.archives-ouvertes.fr/hal-1485166" property="cc:attributionName" rel="cc:attributionURL"> <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"></a>


## More information in [the repository _cachets hittites_](../../../cachets_hittites/)
